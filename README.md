# pcre-rpi1

Patch to apply backward-compatibility with pcre 6.x versions.

See
https://stackoverflow.com/questions/27561492/how-to-get-past-mongodb-pcre-symbol-lookup-error-when-upgrading-from-mongodb-1

https://bugs.launchpad.net/ubuntu/+source/pcre3/+bug/130428

https://bugs.exim.org/show_bug.cgi?id=1275 

pcre-git / commit 4e1d44faf2bbbe055bc66ec1765096c2e175e5a8:

> Import patches-unapplied version 7.2-1ubuntu2 to ubuntu/gutsy
> 
> Imported using git-ubuntu import.
> 
> Changelog parent: 9101cf0c57b915dc4189e7d3a9c6e74395be0daf
> 
> New changelog entries:
>   * Work around an API change; in 7.0, the signature of
>       void RE::Init(const char *pat, const RE_Options* options)
>     did change to
>       void RE::Init(const string& pat, const RE_Options* options)
>    Re-add the old one and let it call the new one. LP: #130428.
	

